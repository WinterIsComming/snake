package com.codenjoy.dojo.snakebattle.client;

import com.codenjoy.dojo.services.Point;

public interface Scenario {
    String prediction(Point nearest, Point point);
}
