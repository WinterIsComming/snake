package com.codenjoy.dojo.snakebattle.client;

import java.util.List;

public class PredictedWay {
    private List<String> way;
    private int wayRanking;

    public PredictedWay(List<String> way, int wayRanking) {
        this.way = way;
        this.wayRanking = wayRanking;
    }

    public List<String> getWay() {
        return way;
    }

    public int size() {
        return way.size();
    }

    public int getWayRanking() {
        return wayRanking;
    }

    public int getEfficiency() {
        return size() - getWayRanking();
    }
}
