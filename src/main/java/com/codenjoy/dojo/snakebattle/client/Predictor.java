package com.codenjoy.dojo.snakebattle.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 - 2019 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.PointImpl;
import com.codenjoy.dojo.snakebattle.model.Elements;

import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.codenjoy.dojo.snakebattle.model.Elements.*;

public class Predictor {
    public static final int MAX_SNAKE_SIZE_FOR_STONE_FLOW = 5;
    public static final int ATTEMPT_THRESHOLD = 40;
    public static final int ATTEMPT_THRESHOLD_FOR_APPLYING_APPLE_FLOW = 30;
    public static final int GOLD_COEFFICIENT = 5;
    public static final int BOMB_COEFFICIENT = 10;
    private Board board;
    private Point me;
    private List<String> allowDirections;
    private static long direction;

    public Predictor(Board board) {
        this.board = board;
        this.me = board.getMe();
        this.allowDirections = getAllowedDirections(me);
    }

    private List<String> getAllowedDirections(Point point) {
        List<String> directions = new ArrayList<>();
        if (!isBarrier(point.getX(), point.getY() - 1) && !board.isAt(point, Elements.HEAD_UP)) {
            directions.add(Direction.DOWN.toString());
        }
        if (!isBarrier(point.getX(), point.getY() + 1) && !board.isAt(point, Elements.HEAD_DOWN)) {
            directions.add(Direction.UP.toString());
        }
        if (!isBarrier(point.getX() - 1, point.getY()) && !board.isAt(point, Elements.HEAD_RIGHT)) {
            directions.add(Direction.LEFT.toString());
        }
        if (!isBarrier(point.getX() + 1, point.getY()) && !board.isAt(point, Elements.HEAD_LEFT)) {
            directions.add(Direction.RIGHT.toString());
        }
        if (directions.isEmpty()) {
            System.out.println("EMPTY DIRECTIONS");
            directions.add(Direction.RIGHT.toString());
        }
        return directions;
    }

    public String getDirection() {
        Point nearestGolds = getNearestElement(Elements.GOLD);
        Point nearestApple = getNearestElement(Elements.APPLE);
        Point nearestStone = getNearestElement(Elements.STONE);
        List<PredictedWay> ways = new ArrayList<>();
        if (nearestGolds != null) {
            ways.add(getPredictedWay(nearestGolds));
        }

        if (nearestApple != null) {
            ways.add(getPredictedWay(nearestApple));
        }

        System.out.println("SNAKKEFIZE " + getSnakeSize());
        if (nearestStone != null && enableEatStone()) {
            System.out.println("BOMB FLOW");
            ways.add(getPredictedWay(nearestStone));
        }

        PredictedWay predictedWay = ways.stream().sorted((firstWay, secondWay) -> firstWay.getEfficiency() < secondWay.getEfficiency() ? -1 : 1).findFirst().orElse(null);
        if (predictedWay == null) {
            return Direction.RIGHT.toString();
        }

        if (predictedWay.size() > ATTEMPT_THRESHOLD_FOR_APPLYING_APPLE_FLOW) {
            List<Point> points = board.get(Elements.APPLE);
            List<PredictedWay> newWays = new ArrayList<>();
            newWays.add(predictedWay);

            for (Point point : points) {
                newWays.add(getMorePreferableWay(getPredictedWays(point)));
            }

            predictedWay = getMorePreferableWay(newWays);
        }

        System.out.println("PREDICTED WAY: " + predictedWay.getWay());
        System.out.println("PREDICTED WAY EFFICIENCY: " + predictedWay.getEfficiency());
        System.out.println("PREDICTED WAY RANKING: " + predictedWay.getWayRanking());
        System.out.println("PREDICTED WAY SIZE: " + predictedWay.size());
        return predictedWay.size() > 0 ? predictedWay.getWay().get(0) : Direction.RIGHT.toString();
    }

    private PredictedWay getMorePreferableWay(List<PredictedWay> ways) {
        ways.removeIf(way -> way.getWay().isEmpty());
        if (ways.isEmpty()) {
            System.out.println("NO WAY TO GO");
            return null;
        }
        PredictedWay morePreferable = ways.get(0);
        for (PredictedWay way : ways) {
            if (way.size() < morePreferable.size()) {
                morePreferable = way;
            }
        }
        return morePreferable;
    }


    private List<PredictedWay> getPredictedWays(Point nearest) {
        Map<String, Scenario> scenarios = initScenario();
        List<PredictedWay> ways = new ArrayList<>();
        scenarios.values().forEach(scenario -> {
            ways.add(new PredictedWay(callPrediction(nearest, scenario), getRanking(nearest)));
        });
        return ways;
    }

    private int getRanking(Point point) {
        if (board.isAt(point, Elements.APPLE)) {
            return 1;
        }
        if (board.isAt(point, Elements.GOLD)) {
            return GOLD_COEFFICIENT;
        }
        if (board.isAt(point, Elements.STONE)) {
            return BOMB_COEFFICIENT;
        }
        return 0;
    }

    private PredictedWay getPredictedWay(Point nearest) {
        List<PredictedWay> ways = getPredictedWays(nearest);
        return getMorePreferableWay(ways);
    }

    private List<String> callPrediction(Point nearest, Scenario scenario) {
        List<String> steps = new ArrayList<>();
        if (nearest == null) {
            return steps;
        }
        Point point = me;
        int count = 0;
        while (!point.equals(nearest) && count < ATTEMPT_THRESHOLD) {
            count++;
            String newDirection = scenario.prediction(nearest, point);
            steps.add(newDirection);
            point = createPredicablePoint(point, newDirection);
        }
        return steps;
    }


    private Point getNearestElement(Elements element) {
        List<Point> golds = getReacheableElements(element);
        return golds.stream().sorted(this::sort).findFirst().orElse(null);
    }

    private List<Point> getReacheableElements(Elements element) {
        List<Point> golds = board.get(element);
        removeUnreachablePoints(golds);
        return golds;
    }

    private Map<String, Scenario> initScenario() {
        Map<String, Scenario> scenarios = new HashMap<>();

        scenarios.put("right", (nearest, point) -> {
            List<String> desirableDirection = getDesirableDirection(nearest, me);
            List<String> allDirection = getAllowedDirections(point);

            return rightDirection(desirableDirection, allDirection);
        });

        scenarios.put("back", (nearest, point) -> {
            List<String> desirableDirection = getDesirableDirection(nearest, me);
            List<String> allDirection = getAllowedDirections(point);

            return backDirection(desirableDirection, allDirection);
        });

        scenarios.put("combine", new Scenario() {
            private boolean change;

            @Override
            public String prediction(Point nearest, Point point) {
                List<String> desirableDirection = getDesirableDirection(nearest, me);
                List<String> allDirection = getAllowedDirections(point);
                change = !change;
                return change ? rightDirection(desirableDirection, allDirection) : backDirection(desirableDirection, allDirection);
            }
        });

        scenarios.put("combineDivideThree", new Scenario() {
            private int counter;

            @Override
            public String prediction(Point nearest, Point point) {
                List<String> desirableDirection = getDesirableDirection(nearest, me);
                List<String> allDirection = getAllowedDirections(point);
                counter = counter++;
                return counter % 3 == 0 ? rightDirection(desirableDirection, allDirection) : backDirection(desirableDirection, allDirection);
            }
        });


        return scenarios;
    }

    private String backDirection(List<String> desirableDirection, List<String> allowDirection) {
        for (int i = desirableDirection.size() - 1; i >= 0; i--) {
            if (allowDirection.contains(desirableDirection.get(i))) {
                return desirableDirection.get(i);
            }
        }
        return allowDirection.get(0);
    }

    private String rightDirection(List<String> desirableDirection, List<String> allowDirection) {
        for (String direction : desirableDirection) {
            if (allowDirection.contains(direction)) {
                return direction;
            }
        }
        return allowDirection.get(0);
    }

    private Point createPredicablePoint(Point point, String direction) {
        int x = Direction.RIGHT.toString().equals(direction) ? point.getX() + 1 : Direction.LEFT.toString().equals(direction) ? point.getX() - 1 : point.getX();
        int y = Direction.UP.toString().equals(direction) ? point.getY() + 1 : Direction.DOWN.toString().equals(direction) ? point.getY() - 1 : point.getY();
        ;

        return new PointImpl(x, y);
    }

    private List<String> getDesirableDirection(Point nearest, Point point) {
        List<String> desirableDirection = new ArrayList<>();
        if (nearest == null) {
            return desirableDirection;
        }
        if (nearest.getY() < point.getY()) {
            desirableDirection.add(Direction.DOWN.toString());
        }
        if (nearest.getY() > point.getY()) {
            desirableDirection.add(Direction.UP.toString());
        }
        if (nearest.getX() < point.getX()) {
            desirableDirection.add(Direction.LEFT.toString());
        }
        if (nearest.getX() > point.getX()) {
            desirableDirection.add(Direction.RIGHT.toString());
        }
        return desirableDirection;
    }


    private int getSnakeSize() {
        List<Point> snakeParts = board.get(
                Elements.BODY_HORIZONTAL, Elements.BODY_LEFT_DOWN, Elements.BODY_LEFT_UP, Elements.BODY_RIGHT_DOWN, Elements.BODY_RIGHT_UP,
                Elements.BODY_VERTICAL);
        return snakeParts.size();
    }

    private void removeUnreachablePoints(List<Point> points) {
        points.removeIf(point -> {
            int x = point.getX();
            int y = point.getY();
            return isWayToTrap(x, y) || checkSnakeNearToGoal(x, y);
        });
    }


    private int sort(Point point1, Point point2) {
        return Double.valueOf(me.distance(point1)).intValue() - Double.valueOf(me.distance(point2)).intValue();
    }

    private boolean isBarrier(int x, int y) {
        return board.isBarrierAt(x, y) || isStoneFlow(x, y) || board.isOutOfField(x, y) || itsMe(x, y)
                || isEnemy(x, y) || isWall(x, y) || isWayToTrap(x, y) || checkSnakeNearToGoal(x, y);
    }

    private boolean isAnotherSnakeAttackTheSamePoint(int x, int y) {
        return board.isAt(x, y, Elements.ENEMY_HEAD_DOWN, Elements.ENEMY_HEAD_UP, Elements.ENEMY_HEAD_EVIL, Elements.ENEMY_HEAD_FLY, Elements.ENEMY_HEAD_LEFT, Elements.ENEMY_HEAD_RIGHT);
    }

    private boolean checkSnakeNearToGoal(int x, int y) {
        return isAnotherSnakeAttackTheSamePoint(x + 1, y) || isAnotherSnakeAttackTheSamePoint(x - 1, y) ||
                isAnotherSnakeAttackTheSamePoint(x, y + 1) || isAnotherSnakeAttackTheSamePoint(x, y - 1);
    }

    private boolean isStoneFlow(int x, int y) {
        if (enableEatStone()) {
            return false;
        }
        return board.isStoneAt(x, y);
    }

    private boolean enableEatStone() {
        return getSnakeSize() > MAX_SNAKE_SIZE_FOR_STONE_FLOW;
    }

    private boolean isWall(int x, int y) {
        return board.isAt(x, y, WALL, START_FLOOR);
    }

    private boolean isWayToTrap(int x, int y) {
        return (board.isAt(x + 1, y, WALL, START_FLOOR, ENEMY_TAIL_INACTIVE) && board.isAt(x - 1, y, WALL, START_FLOOR, ENEMY_TAIL_INACTIVE)) ||
                (board.isAt(x, y + 1, WALL, START_FLOOR, ENEMY_TAIL_INACTIVE) && board.isAt(x, y - 1, WALL, START_FLOOR, ENEMY_TAIL_INACTIVE));
    }

    private boolean itsMe(int x, int y) {
        return board.isAt(x, y, Elements.BODY_HORIZONTAL, Elements.BODY_LEFT_DOWN, Elements.BODY_LEFT_UP, Elements.BODY_RIGHT_DOWN, Elements.BODY_RIGHT_UP,
                Elements.BODY_VERTICAL);
    }

    private boolean isEnemy(int x, int y) {
        return board.isAt(x, y, ENEMY_BODY_HORIZONTAL, ENEMY_BODY_VERTICAL, ENEMY_BODY_LEFT_UP,
                ENEMY_BODY_LEFT_DOWN, ENEMY_BODY_RIGHT_DOWN, ENEMY_BODY_RIGHT_UP, ENEMY_HEAD_SLEEP, ENEMY_TAIL_INACTIVE, TAIL_INACTIVE,
                ENEMY_TAIL_END_DOWN, ENEMY_TAIL_END_UP, ENEMY_TAIL_END_LEFT, ENEMY_TAIL_END_RIGHT);
    }
}
